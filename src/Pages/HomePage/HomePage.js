import React, { Fragment } from 'react';
import Home from './../../Components/Home/Home';

const HomePage = () => {
    return (
        <Fragment>
            {/* HOME-CONTENT */}
            <Home />
        </Fragment>
    )
}

export default HomePage
