/** all pages are imported to routes */
import React from 'react';
import HomePage from './../Pages/HomePage/HomePage';

const Routes = [
    {
        path: "/",
        exact: true,
        main: () => <HomePage />,
    }
];
export default Routes;