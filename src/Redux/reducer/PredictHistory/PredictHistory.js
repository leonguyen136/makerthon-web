import * as Types from '../../../Constants/ActionTypes';

const PredictHistory = (state = [], action) => {
    switch (action.type) {
        case Types.PREDICT_HISTORY:
            return action.predictHistory;
        default: return [...state];
    }
}

export default PredictHistory