import * as Types from '../../../Constants/ActionTypes';

const PredictTransfer = (state = [], action) => {
    switch (action.type) {
        case Types.PREDICT_TRANSFER:
            return action.predictTransfer;
        default: return [...state];
    }
}

export default PredictTransfer