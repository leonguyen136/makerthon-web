import { combineReducers } from 'redux';
import PredictHistory from './PredictHistory/PredictHistory';
import PredictTransfer from './PredictTransfer/PredictTransfer';

const rootReducers = combineReducers(
    {
        PredictHistory,
        PredictTransfer
    }
);

export default rootReducers;