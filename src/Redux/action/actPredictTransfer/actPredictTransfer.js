import * as Types from '../../../Constants/ActionTypes';

export const actRequestPredictTransfer = (predictTransfer) => {
    return {
        type: Types.PREDICT_TRANSFER,
        predictTransfer
    }
}