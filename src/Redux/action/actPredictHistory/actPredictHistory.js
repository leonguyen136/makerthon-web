import * as Types from '../../../Constants/ActionTypes';

export const actRequestPredictHistory = (predictHistory) => {
    return {
        type: Types.PREDICT_HISTORY,
        predictHistory
    }
}