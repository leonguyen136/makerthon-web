import React from 'react';

export const Loading = (status) => {
    if (status) {
        return (
            <div>
                <div className="circle-loading-overlay"></div>
                <div className="circle-loading"></div>
            </div>
        )
    } else {
        return <div></div>
    }
}