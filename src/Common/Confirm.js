import React from 'react';
/** react-confirm-alert */
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

export const Confirm = (title, titleYes, titleNo, content, onYes, onNo) => {
    confirmAlert({
        customUI: ({ onClose }) => {
            var onClickNo = onNo ? onNo : onClose;
            var onClickYes = () => {
                onYes()
                onClose()
            }
            return (
                <div className="confirm__card">
                    <div className="card-header border-success text-center"
                        style={{ margin: '-1px' }}>
                        {title}
                    </div>
                    <div className="card-body text-center">
                        <p className="card-text">{content}</p>
                        <button type="button" onClick={onClickNo} className="btn btn-secondary py-1 px-2 mr-2">{titleNo}</button>
                        <button
                            type="button"
                            className="btn button-custom py-1 px-2"
                            onClick={onClickYes}
                        >
                            {titleYes}
                        </button>
                    </div>
                </div>
            );
        }, closeOnClickOutside: false
    });
}
