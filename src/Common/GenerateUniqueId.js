/** generate unique id */
const GenerateUniqueId = () => {
    // generate number
    const generateRandomNumber = () => {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    // current time with milliseconds
    const date = new Date();
    // generate id
    return date.getTime() + '-' + generateRandomNumber() + '-' + generateRandomNumber();
}

export default GenerateUniqueId
