import axios from 'axios';
import * as Config from './../Constants/Config';

export default function callApi (method = 'GET', data) {
    return axios({
        method: method,
        url: `${Config.API_URL}`,
        data: data,
        headers: { 'content-type': 'application/json' }
    })
}