import React, { Fragment } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
/** react-router-dom */
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
/** react-toastify */
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
/** react-auth */
import Routes from './Routes/Routes';
import Header from './Components/Header/Header';
// import Footer from './Components/Footer/Footer';
/** react-bootstrap */
import { Container, Row } from 'react-bootstrap';

const App = () => {

    /**
     *  Functions
     */
    const showContent = (routes) => {
        var result = null;
        if (routes.length > 0) {
            result = routes.map((route, index) => {
                return (
                    <Route key={index} path={route.path} exact={route.exact} component={route.main} />
                )
            })

        }
        return result;
    }

    /**
     *  Render
     */
    return (
        <Fragment>
            <Router>
                {/* HEADER */}
                <Header />
                <Container>
                    <Row>
                        {/* CONTENT */}
                        <Switch>
                            {showContent(Routes)}
                        </Switch>
                    </Row>
                </Container>
                {/* FOOTER */}
                {/* <Footer /> */}
                <ToastContainer position="top-right" autoClose={1500} hideProgressBar={true} />
            </Router >
        </Fragment>
    );

}

export default App;
