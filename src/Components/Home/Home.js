import React, { useState } from 'react';
/** react-bootstrap */
import { Col, Form, Row, OverlayTrigger, Tooltip } from 'react-bootstrap';
/** confirm-form */
import { Confirm } from '../../Common/Confirm';
/** loading */
import { Loading } from '../../Common/Loading';
/** api */
import callApi from '../../Api/callApi';
/** validation */
import { validation } from '../../Services/Validation';
/** Inform */
import { Inform } from '../../Common/Inform';

const Home = () => {

    /**
     *  useState
     */
    // values to predict
    const [values, setValues] = useState({
        baso: '',
        eos: '',
        mono: '',
        neu: '',
        lym: '',
        wbc: '',
        hct: '',
        hgb: '',
        rbc: '',
        mch: '',
        mchc: '',
        mcv: '',
        mpv: '',
        rdw: '',
        pdw: '',
        plt: '',
        tpttbm: '',
        pct: '',
    })
    // values errors
    const [valueErr, setValueErr] = useState({
        baso: '',
        eos: '',
        mono: '',
        neu: '',
        lym: '',
        wbc: '',
        hct: '',
        hgb: '',
        rbc: '',
        mch: '',
        mchc: '',
        mcv: '',
        mpv: '',
        rdw: '',
        pdw: '',
        plt: '',
        tpttbm: '',
        pct: '',
    })
    // control loading
    const [loading, setLoading] = useState(false);

    /**
     *  Functions
     */
    // handle onchange value
    const onChangeValue = (e) => {
        let { value, name } = e.target;
        setValues({ ...values, [name]: value });
    }

    // handle onBlur
    const handleOnBlur = (e) => {
        let { name, value } = e.target;
        const error = validation(name, value.trim());
        setValueErr(valueErr => ({ ...valueErr, [name]: error }));
    }

    // reset values 
    const resetValues = () => {
        // reset state
        setValues({
            baso: '',
            eos: '',
            mono: '',
            neu: '',
            lym: '',
            wbc: '',
            hct: '',
            hgb: '',
            rbc: '',
            mch: '',
            mchc: '',
            mcv: '',
            mpv: '',
            rdw: '',
            pdw: '',
            plt: '',
            tpttbm: '',
            pct: '',
        });
        // reset value err
        setValueErr({
            baso: '',
            eos: '',
            mono: '',
            neu: '',
            lym: '',
            wbc: '',
            hct: '',
            hgb: '',
            rbc: '',
            mch: '',
            mchc: '',
            mcv: '',
            mpv: '',
            rdw: '',
            pdw: '',
            plt: '',
            tpttbm: '',
            pct: '',
        })
    }

    // handle onsubmit
    const handleOnSubmit = (e) => {
        e.preventDefault();
        let hasError = false;
        for (let key in values) {
            const error = validation(key, values[key])
            if (error) {
                setValueErr(valueErr => ({ ...valueErr, [key]: error }));
                hasError = true;
            }
        }
        if (hasError) return;
        // agree to predict
        const onYes = () => {
            // start loading
            setLoading(true);
            // prepare data to predict
            let data = {
                "baso": values.baso,
                "eos": values.eos,
                "mono": values.mono,
                "neu": values.neu,
                "lym": values.lym,
                "wbc": values.wbc,
                "hct": values.hct,
                "hgb": values.hgb,
                "rbc": values.rbc,
                "mch": values.mch,
                "mchc": values.mchc,
                "mcv": values.mcv,
                "mpv": values.mpv,
                "rdw": values.rdw,
                "pdw": values.pdw,
                "plt": values.plt,
                "tpttbm": values.tpttbm,
                "pct": values.pct,
            }
            // call api create new product cat
            callApi('POST', data).then(res => {
                if (res.data.length > 0 && res.status === 200) {
                    console.log(res);
                    if (res.data[0] === 'yes') {
                        Inform('Kết quả tư vấn', 'Đóng', "Quý khách nên đến bệnh viện chuyên khoa về ung thư để được tư vấn", '');
                    } else {
                        Inform('Kết quả tư vấn', 'Đóng', "Quý khách nên đến bệnh viện chuyên khoa về ung thư để được tư vấn", '');
                    }
                    // reset state
                    // setValues({
                    //     baso: '',
                    //     eos: '',
                    //     mono: '',
                    //     neu: '',
                    //     lym: '',
                    //     wbc: '',
                    //     hct: '',
                    //     hgb: '',
                    //     rbc: '',
                    //     mch: '',
                    //     mchc: '',
                    //     mcv: '',
                    //     mpv: '',
                    //     rdw: '',
                    //     pdw: '',
                    //     plt: '',
                    //     tpttbm: '',
                    //     pct: '',
                    // });
                    // end loading
                    setLoading(false);
                } else {
                    // end loading
                    setLoading(false);
                }
            }).catch(err => {
                // end loading
                setLoading(false);
                // log err
                console.log(err);
            });
        };
        Confirm('Xác nhận', 'Đồng ý', 'Hủy bỏ', `Bạn có chắc chắn muốn được tư vấn?`, onYes, '');
    }

    /**
     *  Render
     */
    return (
        <Col xs={12} sm={12} md={12} lg={12}>
            {/* HOME-PAGE */}
            <div className="home-page">
                <div className="home-page__content">
                    <Form noValidate onSubmit={handleOnSubmit} className="mt-3">
                        <Row>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="#baso" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng bạch cầu hạt ưa base</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>baso</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="baso"
                                            value={values.baso}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.baso}
                                            isValid={!valueErr.baso && values.baso}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.baso}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="#eos" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng bạch cầu hạt ưa acid</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>eos</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="eos"
                                            value={values.eos}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.eos}
                                            isValid={!valueErr.eos && values.eos}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.eos}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="#mono" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng bạch cầu Mono</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>mono</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="mono"
                                            value={values.mono}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.mono}
                                            isValid={!valueErr.mono && values.mono}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.mono}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="#neu" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng bạch cầu hạt trung tính</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>neu</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="neu"
                                            value={values.neu}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.neu}
                                            isValid={!valueErr.neu && values.neu}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.neu}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="%lym" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng bạch cầu lympho</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>lym</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="lym"
                                            value={values.lym}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.lym}
                                            isValid={!valueErr.lym && values.lym}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.lym}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="wbc" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng tế bào bạch cầu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>wbc</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="wbc"
                                            value={values.wbc}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.wbc}
                                            isValid={!valueErr.wbc && values.wbc}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.wbc}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="hct" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Tỷ lệ thể tích khối hồng cầu trên tổng thể tích máu toàn phần</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>hct</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="hct"
                                            value={values.hct}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.hct}
                                            isValid={!valueErr.hct && values.hct}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.hct}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="hgb" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng huyết sắc tố có trong một đơn vị máu toàn phần</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>hgb</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="hgb"
                                            value={values.hgb}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.hgb}
                                            isValid={!valueErr.hgb && values.hgb}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.hgb}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="rbc" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Số lượng hồng cầu có trong một đơn vị máu toàn phần</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>rbc</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="rbc"
                                            value={values.rbc}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.rbc}
                                            isValid={!valueErr.rbc && values.rbc}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.rbc}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="mch" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng huyết sắc tố có trong mỗi hồng cầu, tính bằng công thức MCH = Hb/RBC</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>mch</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="mch"
                                            value={values.mch}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.mch}
                                            isValid={!valueErr.mch && values.mch}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.mch}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="mchc" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Nồng độ huyết sắc tố trung bình có trong một thể tích khối hồng cầu, tính bằng công thức MCHC = Hb/HCT</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>mchc</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="mchc"
                                            value={values.mchc}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.mchc}
                                            isValid={!valueErr.mchc && values.mchc}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.mchc}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="mcv" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Thể tích trung bình của mỗi hồng cầu, tính bằng công thức MCV = HCT/RBC</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>mcv</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="mcv"
                                            value={values.mcv}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.mcv}
                                            isValid={!valueErr.mcv && values.mcv}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.mcv}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="mpv" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Thể tích trung bình của tiểu cầu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>mpv</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="mpv"
                                            value={values.mpv}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.mpv}
                                            isValid={!valueErr.mpv && values.mpv}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.mpv}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="rdw" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Mức độ đồng đều phân bố kích thước giữa các hồng cầu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>rdw</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="rdw"
                                            value={values.rdw}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.rdw}
                                            isValid={!valueErr.rdw && values.rdw}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.rdw}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="pdw" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Mức độ đồng đều phân bố kích thước giữa các tiểu cầu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>pdw</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="pdw"
                                            value={values.pdw}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.pdw}
                                            isValid={!valueErr.pdw && values.pdw}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.pdw}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="plt" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Lượng tiểu cầu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>plt</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="plt"
                                            value={values.plt}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.plt}
                                            isValid={!valueErr.plt && values.plt}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.plt}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="tpttbm" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Đếm số lượng các tế bào máu gồm: số lượng hồng cầu (RBC), bạch cầu (WBC), tiểu cầu (PLT) đồng thời xác định tỷ lệ phần trăm và kích thước các tế bào máu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>tpttbm</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="tpttbm"
                                            value={values.tpttbm}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.tpttbm}
                                            isValid={!valueErr.tpttbm && values.tpttbm}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.tpttbm}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="pct" className="mb-1">
                                        <OverlayTrigger
                                            placement="top-start"
                                            overlay={<Tooltip id="button-tooltip-2">Dung tích khối tiểu cầu</Tooltip>}
                                        >
                                            {({ ref, ...triggerHandler }) => (
                                                <Form.Label {...triggerHandler} ref={ref}>pct</Form.Label>
                                            )}
                                        </OverlayTrigger>
                                        <Form.Control
                                            type="text"
                                            placeholder="Nhập dữ liệu"
                                            name="pct"
                                            value={values.pct}
                                            onChange={onChangeValue}
                                            onBlur={handleOnBlur}
                                            isInvalid={valueErr.pct}
                                            isValid={!valueErr.pct && values.pct}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Control.Feedback type="invalid" className="d-block mx-2 mt-0 mb-2">
                                        {valueErr.pct}
                                    </Form.Control.Feedback>
                                </Form.Row>
                            </Col>
                            <Col xs={3} sm={3} md={3} lg={3}></Col>
                            <Col xs={3} sm={3} md={3} lg={3} className="position-relative">
                                <button
                                    className="btn button-custom button-refresh mr-3"
                                    type="button"
                                    onClick={() => resetValues()}
                                >
                                    <i className="fa fa-eraser"></i>
                                </button>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <div className="home__page__submit">
                                    <button
                                        className="btn button-custom"
                                        type="submit"
                                    >
                                        Tư vấn
                                </button>
                                </div>
                            </Col>
                        </Row>
                        {/* overlay + loading */}
                        {Loading(loading)}
                    </Form>
                </div>
            </div>
        </Col>
    )
}

export default Home
