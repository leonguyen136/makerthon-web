import React from 'react';
/** react-bootstrap */
import { Row, Col } from 'react-bootstrap';

const Footer = () => {

    /**
     *  Render
     */
    return (
        <footer className="footer">
            <div className="footer__content">
                <Row className="mt-2">
                    <Col xs={4} sm={4} md={4} lg={4} className="footer__overview pr-0">

                    </Col>
                    <Col xs={3} sm={3} md={3} lg={3} className="footer__info px-0">

                    </Col>
                    <Col xs={5} sm={5} md={5} lg={5} className="footer__contact">

                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>

                    </Col>
                </Row>
            </div>
        </footer>
    )
}

export default Footer;
