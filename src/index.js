import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import REDUX
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
// import REDUCER
import rootReducers from './Redux/reducer/index';

// Enhance to help import more than 2 params into createStore()
const enhancer = compose(
    applyMiddleware(thunk)
);

const store = createStore(
    rootReducers,
    enhancer
);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));

serviceWorker.unregister();