// -----------------------------------------------------------------------------
// This file contains validation for all from in this project.
// -----------------------------------------------------------------------------
// Validation for all ICD form
export const validation = (name, value) => {
    let icdList = ["baso", "eos", "mono", "neu", "lym", "wbc", "hct", "hgb", "rbc", "mch", "mchc", "mcv", "mpv", "rdw", "pdw", "plt", "tpttbm", "pct"];
    if (icdList.includes(name)) {
        if (!value) {
            return value ? "" : `${name.toUpperCase()} không được để trống`;
        }
        if (!/^[+-]?((\.\d+)|(\d+(\.\d+)?))$/.test(value)) {
            return `${name.toUpperCase()} phải là số`;
        }
    }
    return "";
}
